from trainerbase.main import run

from gui import run_menu
from injections import stat_update_hook, update_last_used_item_count_pointer
from scripts import player_pointer_script_engine, regeneration_script_engine


def on_initialized():
    update_last_used_item_count_pointer.inject()
    stat_update_hook.inject()
    regeneration_script_engine.start()
    player_pointer_script_engine.start()


def on_shutdown():
    regeneration_script_engine.stop()
    player_pointer_script_engine.stop()


if __name__ == "__main__":
    run(run_menu, on_initialized, on_shutdown)
