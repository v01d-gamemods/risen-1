from time import sleep
from typing import override

from trainerbase.common.helpers import suppress_memory_exceptions
from trainerbase.common.teleport import Teleport, Vector3
from trainerbase.tts import say

from objects import god_mode_enabled, position


class RisenTeleport(Teleport):
    @override
    def goto(self, label: str) -> None:
        if not god_mode_enabled.value:
            say("God Mode must be enabled!")
            return

        super().goto(label)
        self.keep_position_frozen(time=5)

    @suppress_memory_exceptions
    def keep_position_frozen(self, time: float):
        self._freeze_position()
        sleep(time)
        self._unfreeze_position()

    def _freeze_position(self):
        self.player_x.frozen = self.player_x.value
        self.player_y.frozen = self.player_y.value
        self.player_z.frozen = self.player_z.value

    def _unfreeze_position(self):
        self.player_x.frozen = None
        self.player_y.frozen = None
        self.player_z.frozen = None


tp = RisenTeleport(
    position.x,
    position.y,
    position.z,
    {
        "Pallas": (68.01654296875, 225.345234375, 125.490185546875),
        "Abrax": (108.534267578125, 287.23947265625, 120.489931640625),
        "Ignatius": (127.8594921875, 272.60201171875, 133.490205078125),
        "Severin": (16.84947998046875, 102.457080078125, 69.3130029296875),
        "Tristan": (-96.70703125, 168.71748046875, 62.7365966796875),
        "Jan": (-218.67294921875, 139.458515625, 79.7121435546875),
        "Ricardo": (-289.2278125, 333.936640625, 85.57658203125),
        "Esteban": (-268.97125, 417.5771875, 87.33728515625),
        "Rhobart": (-182.9505078125, 334.581484375, 105.278408203125),
        "Eastern Temple": (336.6270703125, 143.748671875, 34.7690576171875),
        "Leto": (79.1760107421875, -165.1139453125, 20.08443115234375),
        "Jasmin": (313.3324609375, 216.632734375, 48.4374853515625),
        "Eldric": (298.88671875, 347.769140625, 57.4699609375),
        "Eldric's Prison": (126.54912109375, 450.3514453125, 65.1491455078125),
        "Gyrger": (343.5759375, 418.0405859375, 31.84734619140625),
        "Belschwur": (-43.6661572265625, -147.18240234375, 12.7823095703125),
        "Carasco": (-14.982012939453125, -253.32513671875, 4.91231201171875),
        "Sonya": (-101.062158203125, -186.75671875, 7.943436889648438),
        "Carlos": (-131.453232421875, -97.928310546875, 31.4166015625),
        "Scordo": (-115.303134765625, -183.1653125, 8.122853393554688),
        "Walter/Alvaro": (-136.971865234375, -177.9990625, 11.533182373046875),
        "Nameless Grave 0": (-341.9416015625, 184.265, 97.157138671875),
        "Nameless Grave 1": (-230.922265625, -110.416064453125, 10.549169921875),
        "Nameless Grave 2": (132.190703125, -303.82390625, 2.664541015625),
        "Nameless Grave 3": (-61.19685546875, 214.0910546875, 88.021083984375),
        "Nameless Grave 4": (75.16744140625, -34.01689208984375, 51.6278515625),
        "Nameless Grave 5": (-418.071796875, 205.543984375, 45.3901171875),
        "Patty's Prison": (-495.423828125, 203.17189453125, 37.3972119140625),
        "Patty's Crystal Disk": (-494.6296484375, 430.2015234375, 29.39121337890625),
        "Ursegor": (-88.124609375, 448.32609375, 101.248447265625),
        "Titan Helmet": (-33.206015625, 666.098203125, 105.45541015625),
        "Titan Hammer": (-250.6696484375, 558.412109375, 80.7261279296875),
        "Titan Shield": (39.09010986328125, 631.52984375, 115.832119140625),
        "Titan Armor": (-1.6155923461914062, 269.208984375, 73.7830712890625),
        "Titan Boots": (-273.87439453125, 286.98248046875, 36.78439697265625),
        "Titan": (-0.9833317565917969, 391.0488671875, 102.68998046875),
    },
    minimal_movement_vector_length=0.4,
    dash_coefficients=Vector3(4, 4, 4),
)
