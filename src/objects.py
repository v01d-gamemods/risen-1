from types import SimpleNamespace

from trainerbase.gameobject import (
    GameBool,
    GameFloat,
    GameInt,
    GameUnsignedInt,
    GameUnsignedLongLong,
    GameUnsignedShort,
)

from memory import (
    god_mode_enabled_pointer,
    last_used_item_count_address,
    one_hit_kill_enabled_pointer,
    player_address,
    player_pointer,
    player_property_address,
)


xp = GameInt(player_address + [0x2C])
lp = GameInt(player_address + [0x30])

hp = GameInt(player_address + [0x20])
max_hp = GameInt(player_address + [0x34])
extra_hp = GameInt(player_address + [0xBC])
mana = GameInt(player_address + [0x24])
max_mana = GameInt(player_address + [0x38])
extra_mana = GameInt(player_address + [0xC0])
strength = GameInt(player_address + [0x3C])
extra_strength = GameInt(player_address + [0xC4])
dexterity = GameInt(player_address + [0x40])
extra_dexterity = GameInt(player_address + [0xC8])
wisdom = GameInt(player_address + [0x44])
extra_wisdom = GameInt(player_address + [0xCC])

player_coords_struct_address = player_address + [0x10, 0x58, 0x10, 0x40, 0x24C]
position = SimpleNamespace(
    x=GameFloat(player_coords_struct_address),
    y=GameFloat(player_coords_struct_address + 0x8),
    z=GameFloat(player_coords_struct_address + 0x4),
)

combat_levels = SimpleNamespace(
    sword=GameInt(player_address + [0x60]),
    axe=GameInt(player_address + [0x64]),
    staff=GameInt(player_address + [0x68]),
    archery=GameInt(player_address + [0x6C]),
    crossbow=GameInt(player_address + [0x70]),
)

rune_magic_levels = SimpleNamespace(
    seal=GameInt(player_address + [0x74]),
    can_create_magic_scrolls=GameInt(player_address + [0xA0]),
)

crystal_magic_levels = SimpleNamespace(
    fire=GameInt(player_address + [0x78]),
    frost=GameInt(player_address + [0x7C]),
    bullet=GameInt(player_address + [0x80]),
)

crafting_levels = SimpleNamespace(
    alchemy=GameInt(player_address + [0x9C]),
    blacksmith=GameInt(player_address + [0x84]),
    prospecting=GameInt(player_address + [0x88]),
    can_gut_animals=GameInt(player_address + [0xA4]),
)

thievery_levels = SimpleNamespace(
    lock_picking=GameInt(player_address + [0x8C]),
    pocket_picking=GameInt(player_address + [0x90]),
    sneak=GameInt(player_address + [0x94]),
    acrobatics=GameInt(player_address + [0x98]),
)

properties = SimpleNamespace(
    forward_speed_max=GameFloat(player_property_address + [0x38]),
    strafe_speed_max=GameFloat(player_property_address + [0x3C]),
    backward_speed_max=GameFloat(player_property_address + [0x40]),
    fast_modifier=GameFloat(player_property_address + [0x60]),
    sneak_modifier=GameFloat(player_property_address + [0x64]),
    levitation_modifier=GameFloat(player_property_address + [0x68]),
    jump_height=GameFloat(player_property_address + [0xDC]),
    levitation_fall_velocity=GameFloat(player_property_address + [0xF4]),
    levitation_up_velocity=GameFloat(player_property_address + [0xF8]),
    levitation_max_upward_move=GameFloat(player_property_address + [0xFC]),
)

last_used_item_count = GameUnsignedShort(last_used_item_count_address)
last_used_gold_count = GameUnsignedInt(last_used_item_count_address)

player_pointer_object = GameUnsignedLongLong(player_pointer)
god_mode_enabled = GameBool(god_mode_enabled_pointer)
one_hit_kill_enabled = GameBool(one_hit_kill_enabled_pointer)
