from trainerbase.codeinjection import AllocatingCodeInjection, MultipleCodeInjection

from memory import (
    game_dll,
    god_mode_enabled_pointer,
    last_used_item_count_pointer,
    one_hit_kill_enabled_pointer,
    player_pointer,
)


update_last_used_item_count_pointer = MultipleCodeInjection(
    AllocatingCodeInjection(  # use
        game_dll + 0x8CA7E8,
        f"""
            push rax

            mov rax, {last_used_item_count_pointer}
            mov [rax], rdi

            pop rax

            and eax, 0x1FFFFF
            or [rdi + 0x58], rax
            mov rcx, [rbp + 0x10]
            test rcx, rcx
        """,
        original_code_length=16,
    ),
    AllocatingCodeInjection(  # take
        game_dll + 0x8CA70A,
        f"""
            push rax

            mov rax, {last_used_item_count_pointer}
            mov [rax], rbx

            pop rax

            and edx, 0x1FFFFF
            or [rbx + 0x58], rdx
            mov rcx, [r14 + 0x10]
            test rcx, rcx
        """,
        original_code_length=17,
    ),
)

stat_update_hook = AllocatingCodeInjection(
    game_dll + 0x977C3D,
    f"""
        push rbx

        mov rbx, {game_dll + 0x1194930}
        mov rax, [rbx]

        test r14, r14  ; if current stat is not health
        jnz original  ; then use original code

        mov rbx, {player_pointer}
        mov rbx, [rbx]

        test rbx, rbx  ; if player pointer is not resolved
        jz original ;  then use original code

        mov rbx, [rbx]

        cmp rbx, rsi  ; if current entity is player
        je player  ; then use player code
        jmp enemy  ; else use enemy code

        player:

            mov rbx, {god_mode_enabled_pointer}
            mov bl, [rbx]
            test bl, bl
            jz original ;  skip is God Mode is disabled

            mov ebx, [rsi + r14 * 0x4 + 0x20]
            cmp ebx, edi ;  if current hp > new hp
            cmovg edi, ebx  ; set higher value

            jmp original

        enemy:

            mov rbx, {one_hit_kill_enabled_pointer}
            mov bl, [rbx]
            test bl, bl
            jz original ;  skip is One Hit Kill is disabled

            mov ebx, [rsi + r14 * 0x4 + 0x20]
            cmp ebx, edi  ; if current hp < new hp
            jl original

            mov edi, 0
            jmp original

        original:

            mov [rsi + r14 * 0x4 + 0x20], edi

        pop rbx

        test rax, rax
    """,
    original_code_length=15,
)
