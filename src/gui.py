from dearpygui import dearpygui as dpg
from trainerbase.common.keyboard import GameBoolReleaseHotkeySwitch, SimpleHotkeyHandler
from trainerbase.gui.helpers import add_components, simple_trainerbase_menu
from trainerbase.gui.misc import HotkeyHandlerUI, SeparatorUI, TextUI
from trainerbase.gui.objects import GameObjectUI
from trainerbase.gui.scripts import ScriptUI
from trainerbase.gui.speedhack import SpeedHackUI
from trainerbase.gui.teleport import TeleportUI
from trainerbase.process import kill_game_process

from objects import (
    combat_levels,
    crafting_levels,
    crystal_magic_levels,
    dexterity,
    extra_dexterity,
    extra_hp,
    extra_mana,
    extra_strength,
    extra_wisdom,
    god_mode_enabled,
    hp,
    last_used_gold_count,
    last_used_item_count,
    lp,
    mana,
    max_hp,
    max_mana,
    one_hit_kill_enabled,
    properties,
    rune_magic_levels,
    strength,
    thievery_levels,
    wisdom,
    xp,
)
from scripts import regenerate_hp, regenerate_mana
from teleport import tp


@simple_trainerbase_menu("Risen", 800, 450)
def run_menu():
    small_separator = SeparatorUI(empty_lines_before=0)

    with dpg.tab_bar():
        with dpg.tab(label="Main"):
            add_components(
                ScriptUI(regenerate_hp, "Regenerate HP", "F1"),
                ScriptUI(regenerate_mana, "Regenerate Mana", "F2"),
                SeparatorUI(),
            )

            with dpg.group(horizontal=True):
                add_components(
                    HotkeyHandlerUI(GameBoolReleaseHotkeySwitch(god_mode_enabled, "F3"), "Toggle"),
                    GameObjectUI(god_mode_enabled, "God Mode"),
                )

            with dpg.group(horizontal=True):
                add_components(
                    HotkeyHandlerUI(GameBoolReleaseHotkeySwitch(one_hit_kill_enabled, "F4"), "Toggle"),
                    GameObjectUI(one_hit_kill_enabled, "One Hit Kill"),
                )

            add_components(
                SeparatorUI(),
                TextUI("Last used item/gold count"),
                TextUI(
                    "One pointer can point to a short or long value."
                    " Long int is for gold. Short int is for other items."
                ),
                GameObjectUI(
                    last_used_item_count,
                    "Item Count (short)",
                    set_hotkey="Shift+O",
                    default_setter_input_value=999,
                ),
                GameObjectUI(
                    last_used_gold_count,
                    "Gold Count (long)",
                    default_setter_input_value=1100000,
                ),
                SeparatorUI(),
                HotkeyHandlerUI(SimpleHotkeyHandler(kill_game_process, "Ctrl+Alt+K"), "Kill Game Process"),
            )

        with dpg.tab(label="Teleport & Speedhack"):
            add_components(
                TextUI("To apply new coordinates after teleportation, you need to move!"),
                TeleportUI(tp),
                SeparatorUI(),
                SpeedHackUI(),
            )

        with dpg.tab(label="Stats"):
            add_components(
                GameObjectUI(xp, "XP"),
                GameObjectUI(lp, "LP"),
                small_separator,
                GameObjectUI(hp, "HP"),
                GameObjectUI(max_hp, "Max HP"),
                GameObjectUI(extra_hp, "Extra HP"),
                small_separator,
                GameObjectUI(mana, "Mana"),
                GameObjectUI(max_mana, "Max Mana"),
                GameObjectUI(extra_mana, "Extra Mana"),
                small_separator,
                GameObjectUI(strength, "Strength"),
                GameObjectUI(extra_strength, "Extra Strength"),
                small_separator,
                GameObjectUI(dexterity, "Dexterity"),
                GameObjectUI(extra_dexterity, "Extra Dexterity"),
                small_separator,
                GameObjectUI(wisdom, "Wisdom"),
                GameObjectUI(extra_wisdom, "Extra Wisdom"),
            )

        with dpg.tab(label="Main Skills"):
            add_components(
                GameObjectUI(combat_levels.sword, "Sword"),
                GameObjectUI(combat_levels.axe, "Axe"),
                GameObjectUI(combat_levels.staff, "Staff"),
                GameObjectUI(combat_levels.archery, "Archery"),
                GameObjectUI(combat_levels.crossbow, "Crossbow"),
                SeparatorUI(),
                GameObjectUI(rune_magic_levels.seal, "Seal"),
                GameObjectUI(rune_magic_levels.can_create_magic_scrolls, "Can use scrolls"),
                GameObjectUI(crystal_magic_levels.fire, "Fireball"),
                GameObjectUI(crystal_magic_levels.frost, "Frost"),
                GameObjectUI(crystal_magic_levels.bullet, "Magic Bullet"),
            )

        with dpg.tab(label="Other Skills"):
            add_components(
                GameObjectUI(crafting_levels.alchemy, "Alchemy"),
                GameObjectUI(crafting_levels.blacksmith, "Blacksmith"),
                GameObjectUI(crafting_levels.prospecting, "Prospecting"),
                GameObjectUI(crafting_levels.can_gut_animals, "Can gut animals"),
                GameObjectUI(thievery_levels.lock_picking, "Lock picking"),
                GameObjectUI(thievery_levels.pocket_picking, "Pocket picking"),
                GameObjectUI(thievery_levels.sneak, "Sneak"),
                GameObjectUI(thievery_levels.acrobatics, "Acrobatics"),
            )

        with dpg.tab(label="Player Properties"):
            add_components(
                GameObjectUI(properties.forward_speed_max, "Forward Max Speed", default_setter_input_value=800),
                GameObjectUI(properties.strafe_speed_max, "Strafe Max Speed", default_setter_input_value=800),
                GameObjectUI(properties.backward_speed_max, "Backward Max Speed", default_setter_input_value=800),
                GameObjectUI(properties.fast_modifier, "Fast Velocity Modifier", default_setter_input_value=3),
                GameObjectUI(properties.sneak_modifier, "Sneak Velocity Modifier", default_setter_input_value=0.5),
                GameObjectUI(
                    properties.levitation_modifier,
                    "Levitation Velocity Modifier",
                    default_setter_input_value=10,
                ),
                GameObjectUI(properties.jump_height, "Jump Height", default_setter_input_value=280),
                GameObjectUI(
                    properties.levitation_fall_velocity,
                    "Levitation Fall Velocity",
                    default_setter_input_value=400,
                ),
                GameObjectUI(
                    properties.levitation_up_velocity,
                    "Levitation Up Velocity",
                    default_setter_input_value=500,
                ),
                GameObjectUI(
                    properties.levitation_max_upward_move,
                    "Levitation Max Upward Move",
                    default_setter_input_value=1500,
                ),
            )
