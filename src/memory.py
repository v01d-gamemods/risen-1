from trainerbase.gameobject import GameBool
from trainerbase.memory import Address, allocate, get_module_address
from trainerbase.process import pm


script_dll = get_module_address("Script.dll", retry_limit=32)
game_dll = get_module_address("Game.dll")

base_address = Address(pm.base_address + 0x30F38, [0x50, 0x0, 0x40])
player_address = base_address + [0x78]
player_property_address = base_address + [0x20]

last_used_item_count_pointer = allocate()
last_attacked_target_health_pointer = allocate()
player_pointer = allocate()  # For codeinjection
god_mode_enabled_pointer = allocate(GameBool.ctype_size)
one_hit_kill_enabled_pointer = allocate(GameBool.ctype_size)

last_used_item_count_address = Address(last_used_item_count_pointer, [0x58])
