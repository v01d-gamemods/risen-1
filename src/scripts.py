from pymem.exception import MemoryReadError
from trainerbase.common.helpers import regenerate
from trainerbase.gameobject import ReadonlyGameObjectSumGetter
from trainerbase.scriptengine import ScriptEngine, enabled_by_default

from memory import player_address
from objects import extra_hp, extra_mana, hp, mana, max_hp, max_mana, player_pointer_object


regeneration_script_engine = ScriptEngine(delay=1)
player_pointer_script_engine = ScriptEngine(delay=1)


@regeneration_script_engine.simple_script
def regenerate_hp():
    regenerate(hp, ReadonlyGameObjectSumGetter(max_hp, extra_hp), percent=1, min_value=1)


@regeneration_script_engine.simple_script
def regenerate_mana():
    regenerate(mana, ReadonlyGameObjectSumGetter(max_mana, extra_mana), percent=2, min_value=1)


@enabled_by_default
@player_pointer_script_engine.simple_script
def update_player_pointer():
    try:
        current_player_address = player_address.resolve()
    except MemoryReadError:
        current_player_address = 0

    player_pointer_object.value = current_player_address
